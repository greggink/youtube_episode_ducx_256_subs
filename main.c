/*********************************************************
	duCx interpreter Version 0.000004
	Copyright 2021 Gregg Ink
	Zlib license

	To compile:
	gcc -o ducx main.c -lpthread;

**********************************************************/
#include "stdio.h"
#include "tc.h"

#ifndef NULL
#define NULL \0
#endif

#define DUC_MAX_STRING_SIZE 4095
#define DUC_MAX_STRING_SIZE_INC 4096

#ifndef MAX_PATH
#define MAX_PATH 260
// MAX_PATH including NULL character
#define MAX_PATH_INC MAX_PATH + 1
#endif

#define TEC_MOST_SIG_BIT 128
#define TEC_2ND_MOST_SIG_BIT 64

#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>

enum tok_type{DUC_TOK_ERROR, DUC_TOK_FUNC_NAME, DUC_TOK_DATATYPE, DUC_TOK_VAR_NAME, DUC_TOK_STRING, DUC_TOK_NUMBER, DUC_TOK_KEYWORD, DUC_TOK_EOF};

typedef struct _token token;
struct _token{

	int type;
	char *start;
	int len;
	char *str;
	int str_len;

	char *start_next;	//start of next token

};

typedef struct _var vars;
struct _var{

	int type;
	char name[32];	//max 31 character names and NULL character
	union {
		int i;
		float f;
	} value;
	int len;

};

vars v_list[127];

/*tec_file_exists:
	returns 1 if file exists
	returns 0 in all other cases ( doesn't exist or error )
*/
int			tec_file_exists(char *path);

/*tec_file_get_size:
	returns the size of the file in bytes
	returns -1 in case of error e.g. file does not exist
*/
int64_t		tec_file_get_size(char *path);

/*tec_file_get_contents:
	this will return the raw contents of a file in a NULL terminated buffer
	returns NULL in case of error (file does not exist, no read permission, etc.)
	free the returned buffer
*/
char*		tec_file_get_contents(char *path);

/*tec_char_is_white_space:
	returns 1 if c is a white space character (e.g. space)
	returns 0 otherwise
	assumes 7 bit ascii character
	there are more white space characters within unicode
	they are not so commonly used and could not all be considered in just an 8 bit character
*/
int			tec_char_is_white_space(char c);

/*tec_buf_begins:
	returns 1 if the buffer begins with string str
	returns 0 in all other cases, including errors and str being longer than buffer
*/
int			tec_buf_begins(char *buffer, char *str);

/*tec_string_shift:
	removes an ascii char or unicode codepoint at front of string
	assumes a valid utf8 string
*/
void		tec_string_shift(char *str);

/*tec_string_length:
	returns the length of a string in bytes
	check tec_string_utf8_length to know the number of codepoints
	unlike strlen, this function does not segfault when you give it a NULL pointer
	instead it returns zero because, well, you gave it an empty string ;-)
*/
int			tec_string_length(char *str);

/*tec_string_copy:
	safer alternative to strcpy or even strncpy
	int size is the size of the destination
	these functions guarantee that at most size - 1 bytes will be written to destination plus a NULL character
	this prevents buffer overflows
	this guarantees you get a NULL terminated string in destination (unlike strncpy)
	this function will not cut off the copying in the middle of a UTF8 codepoint when out of space
	returns 1 if all was copied right
	returns 2 if source could not be fully copied
	returns 0 in case of error

	these functions assume char *source is a correctly formatted UTF8 string
*/
int			tec_string_copy(char *destination, char *source, int size);

/*tec_string_to_int:
	converts a string to an integer
	string may not contain anything in front of number except '-' or '+'
	does not safeguard against overflow
*/
int			tec_string_to_int(char *s);

/*tec_string_from_int:
	takes an integer and converts it to a string
	writes the result into char* buffer
	returns the number of bytes written (including NULL character)
	returns -1 upon failure
*/
int			tec_string_from_int(int32_t i, char *buffer, int32_t buffer_size);

//////////////////////////////////
// prototypes specific to DuCx

#define duc_print_error(MSG) fprintf(stderr, "\n%sDUCX ERROR: %s%s\n", TC_YEL, TC_NRM, MSG)

void duc_exec_print_string(token *tok);
void duc_exec_print_int(token *tok);
void duc_exec_print_error(token *tok);
void duc_exec_function(token *tok);
token* duc_get_next_token(token *tok);
char* duc_skip_shebang(char *buffer);
char* duc_string_unescape_double_quotes(char *str);
int duc_check_datatype(token *tok);
void duc_create_variable(token *tok);
vars* duc_get_var(token *tok);

void duc_debug_display_token(token *tok);

/////////////////////////////////

int tec_file_exists(char *path){

	if(!path)
		return 0;

	if(access(path, F_OK) != -1){
		return 1;
	}else{
		return 0;
	}

}//tec_file_exists*/

int64_t tec_file_get_size(char *path){

	if(!path)
		return -1;

#ifdef __linux
	struct stat st;
	int error = stat(path, &st);
	if(!error){
		return (int64_t) st.st_size;
	}else{
		return -1;
	}
#endif

#ifdef __WIN32
	BOOL test = FALSE;
	int64_t size;
	HANDLE fp = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	test = GetFileSizeEx(fp, (PLARGE_INTEGER) &size);
	CloseHandle(fp);
	if(test){
		return size;
	}
#endif

	return -1;

}//tec_file_get_size*/

char* tec_file_get_contents(char *path){

	if(!path)
		return NULL;

	int64_t size = tec_file_get_size(path);
	if(size == -1)
		return NULL;

	size += 1;
	FILE *fp = fopen(path, "rb");
	if(!fp){
		return NULL;
	}
	char *buffer = (char *) malloc(sizeof(char) * size);
	fread(buffer, sizeof(char), size, fp);
	fclose(fp);
	buffer[size-1] = 0;
	return buffer;

}//tec_file_get_contents*/

int tec_char_is_white_space(char c){

	if(c == 32 || c == 9 || c == 10 || c == 11 || c == 12 || c == 13)
		return 1;
	return 0;

}//tec_char_is_white_space*/

int tec_buf_begins(char *buffer, char *str){

	if(!buffer)
		return 0;
	if(!str)
		return 0;

	while(*str && *buffer == *str){
		str += 1;
		buffer += 1;
	}

	if(*str){
		return 0;
	}

	return 1;

}//tec_buf_begins*/

void tec_string_shift(char *str){

	if(!str)
		return;

	int len = tec_string_length(str);
	int i = 1;
	int j = 1;

	if( (str[i-j] & TEC_MOST_SIG_BIT) && (str[i-j] & TEC_2ND_MOST_SIG_BIT) ){
		j += 1;
		i += 1;
		while( (str[i] & TEC_MOST_SIG_BIT) && !(str[i] & TEC_2ND_MOST_SIG_BIT) ){
			j += 1;
			i += 1;
		}
	}

	while(i < len){
		str[ i - j ] = str[i];
		i += 1;
	}
	str[i - j] = '\0';

}//tec_string_shift*/

int tec_string_length(char *str){

	if(!str)
		return 0;
	if(!*str)
		return 0;

	int len = 0;

#if __x86_64__

	int64_t *str_i = (int64_t *) str;
	int64_t addr = (int64_t) str_i;

	// 64 bit computer
	// ensure str is on 8-byte boundary before using speed-up trick
	while( addr&0x0000000000000007 && *str ){
		len += 1;
		str += 1;
		addr = (int64_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 8 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str_i = (int64_t *) str;
	while( !( ( *str_i - 0x0101010101010101 ) & ~(*str_i) & 0x8080808080808080 ) ){
		len += 8;
		str_i += 1;
	}

	str = (char *) str_i;
	while(*str){
		len += 1;
		str += 1;
	}

#else

	int32_t *str32_i = (int32_t *) str;
	int32_t addr32 = (int32_t) str32_i;

	// 32 bit computer
	// ensure str is on 4-byte boundary before using speed-up trick
	while( addr32&0x00000003 && *str ){
		len += 1;
		str += 1;
		addr32 = (int32_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 4 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str32_i = (int32_t *) str;
	while( !( ( *str32_i - 0x01010101 ) & ~(*str32_i) & 0x80808080 ) ){
		len += 4;
		str32_i += 1;
	}

	str = (char *) str32_i;
	while(*str){
		len += 1;
		str += 1;
	}

#endif

	return len;

}//tec_string_length*/

int tec_string_copy(char *destination, char *source, int size){

	if(!destination)
		return 0;
	if(!source){
		*destination = 0;
		return 1;
	}
	if(size <= 0)
		return 0;

	size -= 1;

	int i = 0;
	while(*source && i < size){
		destination[i] = *source;
		source += 1;
		i += 1;
	}

	// we don't want to cut off the copying in the middle of a UTF8 codepoint
	// firstly check whether the next byte of source is either not present or the start of a codepoint
	if(*source && (*source & TEC_MOST_SIG_BIT) && !(*source & TEC_2ND_MOST_SIG_BIT) ){
		i -= 1;
		// this while loop goes back while we have the 2nd, 3rd or 4th byte of a UTF8 codepoint
		while( (destination[i] & TEC_MOST_SIG_BIT) && !(destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			i -= 1;
		}
		// this goes back from the head of a UTF8 codepoint
		if( (destination[i] & TEC_MOST_SIG_BIT) && (destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			destination[i] = 0;
		}else{
			// should never happen, this would be invalid UTF8
			destination[i] = 0;
			return 0;
		}
	}

	destination[i] = '\0';

	if(*source){
		return 2;
	}else{
		return 1;
	}

}//tec_string_copy*/

int tec_string_to_int(char *s){

	if(!s)
		return 0;

	int sign = 1;
	int result = 0;

	while(tec_char_is_white_space(*s)){
		s += 1;
	}
	if(*s == '-'){
		sign = -1;
		s += 1;
	}else{
		if(*s == '+'){
			s += 1;
		}
	}

	while(*s){
		if(*s > '9' || *s < '0'){
			return result * sign;
		}
		result *= 10;
		result += *s - '0';
		s += 1;
	}

	return result * sign;

}//tec_string_to_int*/

int tec_string_from_int(int32_t i, char *result, int32_t buffer_size){

	if(!result)
		return -1;

	int tmp = i;
	int len = 1;
	int j = 0;
	int negative = 0;

	if(i < 0){
		negative = 1;
		len += 1;
		tmp *= -1;
	}

	while(tmp > 9){
		tmp /= 10;
		len += 1;
	}

	if(len + 1 > buffer_size){
		result[0] = '\0';
		return -1;
	}

	result[len] = '\0';
	j = len;
	if(negative){
		result[0] = '-';
		i *= -1;
	}
	while(j - negative){
		j -= 1;
		result[j] = i%10 + '0';
		i /= 10;
	}

	return len + 1;	//+1 accounts for NULL character

}//tec_string_from_int*/

int main(int argc, char **argv){

	if(argc < 2){
		duc_print_error("please provide a valid duCx sourcefile\n");
		return 1;
	}

	if(!tec_file_exists(argv[1])){
		duc_print_error("file does not appear to exists or cannot be accessed\n");
		return 3;
	}

	char *buffer = tec_file_get_contents(argv[1]);

	if(!buffer){
		duc_print_error("failed to load the file");
	}

	buffer = duc_skip_shebang(buffer);

	token tok;
	memset(&tok, 0, sizeof(tok));
	tok.start_next = buffer;
	tok.str = (char *) malloc(sizeof(char) * DUC_MAX_STRING_SIZE_INC);
	tok.str[0] = 0;

	while(tok.type != DUC_TOK_EOF){
		duc_get_next_token(&tok);

		switch(tok.type){
		case DUC_TOK_FUNC_NAME:
			duc_exec_function(&tok);
			break;
		case DUC_TOK_DATATYPE:
			duc_create_variable(&tok);
			break;
		}
	}

	return 0;

}//main*/

void duc_exec_print_string(token *tok){

	duc_get_next_token(tok);

	if(tok->type !=  DUC_TOK_STRING ){
		duc_print_error("print_string() should get a string (for now)");
		exit(1);
	}

	if( tok->str_len < DUC_MAX_STRING_SIZE ){
		tok->str[tok->str_len] = '\n';
		tok->str_len += 1;
	}
	write(1, tok->str, tok->str_len);

	//TODO(GI):
	//for now, we assume print_string ends as it should
	tok->start_next = tok->start + tok->len + 2;	//skip over );\n

}//duc_exec_print_string*/

void duc_exec_print_int(token *tok){

	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_NUMBER && tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("print_int() should get an int");
		exit(1);
	}

	if(tok->type == DUC_TOK_VAR_NAME ){
		vars *v = duc_get_var(tok);
		char tmp[12];
		int n = tec_string_from_int( v->value.i, tmp, 12);
		write(1, tmp, n);
	}

	if(tok->type == DUC_TOK_NUMBER ){
		write(1, tok->start, tok->len);
	}

	putchar(10);
	//TODO(GI):
	//for now, we assume print_int ends as it should
	tok->start_next = tok->start + tok->len + 2;	//skip over );\n
	return;

}//duc_exec_print_int*/

void duc_exec_print_error(token *tok){

	duc_get_next_token(tok);

	if(tok->type !=  DUC_TOK_STRING ){
		duc_print_error("print_error() takes a string");
		exit(1);
	}

	if( tok->str_len < DUC_MAX_STRING_SIZE ){
		tok->str[tok->str_len] = '\n';
		tok->str_len += 1;
	}
	write(2, tok->str, tok->str_len);

	//TODO(GI):
	//for now, we assume print_error ends as it should
	tok->start_next = tok->start + tok->len + 2;	//skip over );\n

}//duc_exec_print_error*/

void duc_exec_function(token *tok){

	if( tec_buf_begins(tok->start, "print_string") ){
		duc_exec_print_string(tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_error") ){
		duc_exec_print_error(tok);
		return;
	}

	if( tec_buf_begins(tok->start, "print_int") ){
		duc_exec_print_int(tok);
		return;
	}

	duc_print_error("Unknown function");
	exit(1);

}//duc_exec_function*/

token* duc_get_next_token(token *tok){

	char *buffer = tok->start_next;
	int len = 0;

	tok->str[0] = 0;
	tok->str_len = 0;

	while(*buffer && tec_char_is_white_space(*buffer)){
		buffer += 1;
	}

	if( !(*buffer) ){
		tok->type = DUC_TOK_EOF;
		return tok;
	}

	tok->start = buffer;
	if(*buffer == '\"'){
		// tokenizer finds a string
		char prev = *buffer;
		buffer += 1;
		len += 1;
		while(*buffer && *buffer != '\"'){
			prev = *buffer;
			buffer += 1;
			len += 1;
			if(*buffer && *buffer == '\"' && prev == '\\'){
				buffer += 1;
				len += 1;
			}
		}

		//sanity checks for the strings
		if(!(*buffer)){
			duc_print_error("Unexpected end of file");
			exit(1);
		}
		if(len > DUC_MAX_STRING_SIZE){
			duc_print_error("String is too long!");
			exit(1);
		}

		tec_string_copy(tok->str, tok->start+1, len);
		duc_string_unescape_double_quotes(tok->str);
		buffer += 1;
		len += 1;
		tok->str_len = tec_string_length(tok->str);

		//consider string not ending!!
		tok->len = len;
		tok->type = DUC_TOK_STRING;
		tok->start_next = buffer;
		return tok;

	}

	if(*buffer == '$'){
		// tokenizer finds a variable name
		buffer += 1;
		len += 1;
		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our var can contain numerals 0 -9, just not begin with one
			len += 1;
			buffer += 1;
		}
		tok->len = len;
		tok->type = DUC_TOK_VAR_NAME;
		tok->start_next = buffer;
		return tok;
	}

	if( *buffer >= '0' && *buffer <= '9' ){
		// tokenizer finds a number
		buffer += 1;
		len += 1;
		while( *buffer && *buffer >= '0' && *buffer <= '9' ){
			buffer += 1;
			len += 1;
		}

		tok->len = len;
		tok->type = DUC_TOK_NUMBER;
		tok->start_next = buffer;
		return tok;
	}

	if(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') ) ){
		// tokenizer finds either keyword or function name

		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our function name can contain numerals 0 -9 and underscore, just not begin with one
			len += 1;
			buffer += 1;
		}

		tok->len = len;

		while( tec_char_is_white_space(*buffer) ){
			buffer += 1;
		}

		// TODO(GI):
		// assumption that '(' always follows function name
		// will not be valid for long
		if( *buffer == '(' ){
			tok->type = DUC_TOK_FUNC_NAME;
			tok->start_next = buffer + 1;
			return tok;
		}

		if( duc_check_datatype(tok) ){
			tok->type = DUC_TOK_DATATYPE;
			return tok;
		}

		tok->type = DUC_TOK_KEYWORD;
		tok->start_next = buffer;
		return tok;

	}

	return NULL;

}//duc_get_next_token*/

char* duc_skip_shebang(char *buffer){

	if(buffer[0] == '#' && buffer[1] == '!'){
		buffer += 2;
		while(*buffer && *buffer != '\n'){
			buffer += 1;
		}
		if(!*buffer){
			duc_print_error("Did not find new line at end of shebang line\n");
			exit(1);
		}
		buffer += 1;
	}

	return buffer;

}//duc_skip_shebang*/

char* duc_string_unescape_double_quotes(char *str){

	if(!str)
		return str;

	char *original = str;
	while(*str){
		if(*str == '\\' && str[1] == '\"'){
			tec_string_shift(str);
		}
		str += 1;
	}

	return original;

}//duc_string_unescape_double_quotes*/

int duc_check_datatype(token *tok){

	if(!tok)
		return 0;

	if( tec_buf_begins(tok->start, "int") && tok->len == 3 ){
		return 1;
	}
	if( tec_buf_begins(tok->start, "char") && tok->len == 4 ){
		duc_print_error("char not currently supported\n");
		exit(1);
	}
	if( tec_buf_begins(tok->start, "float") && tok->len == 5 ){
		duc_print_error("float not currently supported\n");
		exit(1);
	}

	return 0;

}//duc_check_datatype*/

void duc_create_variable(token *tok){

	int pointer = 0;
	char *str = tok->start + tok->len;

	while(*str && tec_char_is_white_space(*str)){
		str += 1;
	}

	tok->start_next = str;
	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_VAR_NAME){
		duc_print_error("Invalid declaration of variable");
		exit(1);
	}

//	duc_debug_display_token(tok);
	int hash = (tok->start[1] + tok->start[tok->len-1])%127;

	if( (v_list[hash]).type ){
		hash += 1;
		while(hash >= 127){
			//TODO(GI): prevent infinite loop when array is full
			hash = 0;
		}
	}

	vars *v = &(v_list[hash]);
	if( tec_buf_begins(str, "int") ){
		v->type = 1;
	}
	tec_string_copy(v->name, tok->start, tok->len + 2);

	//check for initialization
	char *eq = tok->start + tok->len;
	while(*eq && tec_char_is_white_space(*eq)){
		eq += 1;
	}
	if(*eq != '='){
		tok->start_next = tok->start + tok->len + 3;//skip over newline
		//TODO(GI): won't work when semi-colon does not follow straight away
		return;
	}

	tok->start_next = eq + 1;
	duc_get_next_token(tok);

	if(tok->type != DUC_TOK_NUMBER){
		duc_print_error("Integers can only be filled in with numbers");
		exit(1);
	}

	v->value.i = tec_string_to_int(tok->start);
	tok->start_next = tok->start + tok->len + 1;

}//duc_create_variable*/

vars* duc_get_var(token *tok){

	int hash = (tok->start[1] + tok->start[tok->len-1])%127;

	return &(v_list[hash]);

}//duc_get_var*/

void duc_debug_display_token(token *tok){

	if(!tok)
		return;

	char *tmp = tok->start;
	int len = tok->len;
	printf("token is *%s", TC_RED);
	while(len){
		putchar(*tmp);
		len -= 1;
		tmp += 1;
	}
	printf("%s*\n", TC_NRM);

}//duc_debug_display_token*/
